# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'


'''
	This program reads the tweets and the top keywords of each user, performs an elastic search of the keywords on
	Wikipedia using the corresponding tweets as a context, re-scores the Wikipedia pages using their elastic scores and
	the TfIdf scores of the corresponding keywords used in the search, sorts the Wikipedia pages by the new score and
	gets the top M Wikipedia pages for the user, collects all the categories of these pages and all their ancestors,
	sorts the categories by their frequency counts, and finally stores the top N categories for each user as one
	document in a specified mongo collection in a specified mongo database.
'''


import time
import itertools
import threading
import multiprocessing
from pprint import pprint
from pymongo import MongoClient
from collections import defaultdict
from elasticsearch import Elasticsearch
from pymongo.errors import BulkWriteError


elastic_api = 'http://10.13.13.32:9200'
mongo_api = 'mongodb://10.13.13.32:27017'

wiki_db = 'wiki20151002'
topics_db = 'topics'

wiki_collection = 'categories_ancestors'
tweets_collection = 'user_recent_tweets'
keywords_collection = 'user_top_keywords'
categories_collection = 'user_top_categories'


topPageCount = 30
topCategoryCount = 100



def categories_to_topcategories(categories, top_category_count=topCategoryCount):
	'''
	This method takes a list of categories, sorts them by their frequency count, and returns the top "top_category_count"
	many categories.

	:param categories: A list of categories.
	:param top_category_count: Number of top categories to return.

	:return: A list containing a dictionary of the frequency counts of the categories and the top categories of a given user.
	'''
	# create a dictionary of frequency counts of the categories in "categories"
	categories_dict = defaultdict(int)
	for category in categories:
		categories_dict[category] += 1
	# sort the categories by their frequency counts and return the top "top_category_count" many of them
	sorted_dict = sorted(categories_dict, key=categories_dict.get, reverse=True)
	top_categories = sorted_dict[:top_category_count]
	# return the top categories and the frequency counts for all categories for a given user
	return [categories_dict, top_categories]


def pages_to_categories(collection_wiki, pagesSorted):
	'''
	This method takes a list of Wikipedia pages and returns a list of all ancestors of their categories.

	:param collection_wiki: Access client to a mongo collection that stores the category hierarchy of Wikipedia.
	:param pagesSorted: A list of Wikipedia pages.

	:return: A list of all ancestors of all categories occurring in any of the Wikipedia pages in "pagesSorted".
	'''
	# collect all the categories occurring in any of the Wikipedia pages in "pagesSorted"
	categories = [page['_source']['category'] for page in pagesSorted]
	combined_categories = list(itertools.chain(*categories))
	combined_categories = [category.lower().replace('.', '_') for category in combined_categories]

	# find all ancestors of all the categories obtained above from the mongo collection "collection_wiki"
	fd = list(collection_wiki.find({'category': {'$in': combined_categories}}))

	cat = []
	[cat.extend(entry['ancestors_list']) for entry in fd]

	return cat


def tfidf_normalize_scores_of_wikipages(pages, tfidfScore):
	'''
	This method takes the normalized elastic search score of the Wikipedia pages and multiplies them with the TfIdf score
	of the corresponding keyword to get a new score.

	:param pages: A list of Wikipedia pages along with their elastic search scores.
	:param tfidfScore: The TfIdf score of a given keyword.

	:return: A list of Wikipedia pages with updated scores.
	'''
	for page in pages:
		page['_score'] = page['_score'] * tfidfScore
	return pages


def normalize_scores_of_wikipages(pages):
	'''
	This method computes a normalized score between 0 and 1 for a given list of Wikipedia pages and their elastic search scores.

	:param pages: A list of Wikipedia pages along with their elastic search scores.

	:return: A list of Wikipedia pages with updated scores.
	'''
	sum = 0
	for page in pages:
		sum = sum + page['_score']
	for page in pages:
		page['_score'] = page['_score'] / sum
	return pages


def get_top_pages_for_user(es_client, tweets_user, keywords_user, top_page_count=topPageCount):
	'''
	This method computes the top Wikipedia pages for a given user. It does an elastic search query of each top keyword
	of the user on Wikipedia using the tweets of the user containing that keyword as a context. Then it re-scores the
	Wikipedia pages using their elastic search scores and the TfIdf scores of the corresponding keywords. Finally, this
	method sorts the Wikipedia pages using the new scores and returns the top "top_page_count" many Wikipedia pages
	for the given user.

	:param es_client: Access client to elastic search.
	:param tweets_user: Access client to the mongo collection containing user tweets.
	:param keywords_user: Access client to the mongo collection containing user keywords.
	:param top_page_count: Number of top Wikipedia pages to return.

	:return: A list of Wikipedia pages.
	'''
	pages_and_normalizedscores = []
	# go through each keyword of the user
	for entry in keywords_user:
		# set the keyword as a search keyword
		keyword = entry[0]
		# get the TfIdf score of the keyword
		tfidfScore = entry[1]
		# build a context for the keyword using the tweets of the user that contains the keyword
		context = list(itertools.chain.from_iterable([[x for x in tweet if x != keyword] \
		                                              for tweet in tweets_user if len(tweet) > 1 and keyword in tweet]))
		# reduce the size of the context if it grows too big as elastic search does not allow a large context
		context = ','.join(context[:min(100, len(context))])
		# search the Wikipedia elastic database for the keyword and the context
		res = es_client.search(index=wiki_db,
								body={
									'query': {
										'bool': {
											'must': {'match': {'page.title': keyword}},
											'should': {
												'multi_match': {
													'query': context,
													'type': 'most_fields',
													'fields': ['page.title', 'page.text'],
													'minimum_should_match': '50%'
												}
											}
										}
									}
								})
		# from the results of the elastic search, discard pages that are redirects
		pages = [hit for hit in res['hits']['hits'] if hit['_source']['category'] and not hit['_source']['redirect']]
		if len(pages) > 0:
			# normalize the scores of the pages to a number between 0 and 1 for comparison with the results of other queries
			normalizedPages = normalize_scores_of_wikipages(pages)
			# use the TfIdf score of the keyword to re-score the Wikipedia pages
			tfidfNormalizedPages = tfidf_normalize_scores_of_wikipages(normalizedPages, tfidfScore)
			# store the pages along with their new scores in the list "pages_and_normalizedscores"
			pages_and_normalizedscores.extend(tfidfNormalizedPages)

	# sort the Wikipedia pages by their scores and return the top "top_page_count" many of them
	pagesSorted = sorted(pages_and_normalizedscores, key=lambda x: x['_score'], reverse=True)
	return pagesSorted[:top_page_count]


def update_top_categories_for_user_parallel(es_client, collection_wiki, actorId, actorName, actorScreenName, tweets_user, keywords_user, top_page_count, top_category_count, queue):
	'''
	This is a wrapper function to compute the top Wikipedia pages and the top categories for a given user in a parallel thread and collect the results in a queue.
	'''
	pagesSorted = get_top_pages_for_user(es_client, tweets_user, keywords_user, top_page_count)
	categories = pages_to_categories(collection_wiki, pagesSorted)
	[categories_dict, top_categories] = categories_to_topcategories(categories, top_category_count)
	queue.put([actorId, actorName, actorScreenName, categories, categories_dict, top_categories])


def update_top_categories_for_user(es_client, collection_wiki, tweets_user, keywords_user, top_page_count=topPageCount, top_category_count=topCategoryCount):
	'''
	This is a wrapper function to compute the top Wikipedia pages and the top categories for a given user.
	'''
	pagesSorted = get_top_pages_for_user(es_client, tweets_user, keywords_user, top_page_count)
	categories = pages_to_categories(collection_wiki, pagesSorted)
	[categories_dict, top_categories] = categories_to_topcategories(categories, top_category_count)
	return [categories, categories_dict, top_categories]


def isplit(iterable, splitters):
	return [list(g) for k, g in itertools.groupby(iterable, lambda x:x in splitters) if not k]


def access_elastic_database(url=''):
	'''
	This method returns access to an elastic index.

	:param url: The url of the elastic index.

	:return: An access client to the elastic index at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	es_client = Elasticsearch(url)
	return es_client


def access_mongo_collection(client, database, collection):
	'''
	This method returns access to a mongo collection.

	:param client: An access client to a mongo database.
	:param database: Name of the mongo database.
	:param collection: Name of the mongo collection.

	:return: An access client to the specified mongo collection in the specified mongo database.
	'''
	return client[database][collection]


def access_mongo_database(url=''):
	'''
	This method returns a mongo client.

	:param url: The url of the mongo database.

	:return: An access client to the mongo database at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	if url:
		client = MongoClient(url)
	else:
		client = MongoClient()
	return client


def update_top_categories_for_all_users_parallel(top_page_count=topPageCount, top_category_count=topCategoryCount):
	'''
	This method collects the tweets and the top keywords of each user, performs an elastic search of the keywords on
	Wikipedia using the corresponding tweets as a context, re-scores the Wikipedia pages using their elastic scores and
	the TfIdf scores of the corresponding keywords, sorts the Wikipedia pages by the new score and gets the top
	"top_page_count" many Wikipedia pages for the user, collects all the categories of these pages and all their ancestors,
	sorts the categories by their frequency counts, and stores the top "top_category_count" many categories for each user
	in a mongo collection (specified here by "collection_category"). This is a parallel implementation of the method
	"update_top_categories_for_all_users". Here the users are processed in batches, and in each batch a new thread is
	created for each user so that the processing of all the users in the current batch can be done in parallel.

	:param top_page_count: Number of top Wikipedia pages to consider.
	:param top_category_count: Number of top categories to return.

	:return: NA
	'''
	client = access_mongo_database(mongo_api)
	collection_wiki = access_mongo_collection(client, wiki_db, wiki_collection)

	collection_tweet = access_mongo_collection(client, topics_db, tweets_collection)
	collection_keyword = access_mongo_collection(client, topics_db, keywords_collection)
	collection_category = access_mongo_collection(client, topics_db, categories_collection)

	es_client = access_elastic_database(elastic_api)

	# define a queue required for collecting results of all threads
	manager = multiprocessing.Manager()
	queue = manager.Queue()

	m = 0
	batch_size = 50
	condition = True

	print 'Inserting into the Mongo collection'
	while condition:
		# get a batch of "batch_size" many users in each iteration from the mongo collection "collection_keyword"
		cursor = collection_keyword.find().skip(m * batch_size).limit(batch_size)
		if cursor.count(with_limit_and_skip=True) < batch_size:
			condition = False

		m = m + 1
		# if m >= 1:
		# 	condition = False

		threads = []
		# go through each user in the current batch
		for entry in cursor:
			# get the user details
			actorId = entry['actorId']
			actorName = entry['actorName']
			actorScreenName = entry['actorScreenName']
			# get a list containing a list of keywords for each tweet of the user
			tweets_user = isplit(collection_tweet.find_one({'actorId': actorId})['keyWords'], '#')
			# get the top keywords for the user along with their TfIdf scores
			keywords_user = collection_keyword.find_one({'actorId': actorId})['top_keywords']
			# create a new thread for the user that computes the relevant categories and returns the top "top_category_count" many categories for the user
			threads.append(threading.Thread(target=update_top_categories_for_user_parallel, args=(es_client, collection_wiki, actorId, actorName, \
																										actorScreenName, tweets_user, keywords_user, top_page_count, top_category_count, queue)))

		# start all the threads
		for i in xrange(len(threads)):
			threads[i].start()

		# wait for all the threads to finish
		for i in xrange(len(threads)):
			threads[i].join()

		# collect the results of all the threads together in the list "values"
		values = []
		while not queue.empty():
			entry = queue.get()
			actorId = entry[0]
			actorName = entry[1]
			actorScreenName = entry[2]
			categories = entry[3]
			categories_dict = entry[4]
			top_categories = entry[5]
			# values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'categories': categories, 'categoriesFrequency': categories_dict, 'topCategories': top_categories})
			values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'topCategories': top_categories})

		# store the relevant information in the mongo collection "collection_category" for all the users in the current batch
		if values:
			try:
				collection_category.insert_many(values)
			except BulkWriteError as bwe:
				pprint(bwe.details)

	print 'Indexing the Mongo collection'
	collection_category.create_index([('actorId', 1)])
	collection_category.create_index([('actorScreenName', 1)])

	client.close()


def update_top_categories_for_all_users(top_page_count=topPageCount, top_category_count=topCategoryCount):
	'''
	This method collects the tweets and the top keywords of each user, performs an elastic search of the keywords on
	Wikipedia using the corresponding tweets as a context, re-scores the Wikipedia pages using their elastic scores and
	the TfIdf scores of the corresponding keywords, sorts the Wikipedia pages by the new score and gets the top
	"top_page_count" many Wikipedia pages for the user, collects all the categories of these pages and all their ancestors,
	sorts the categories by their frequency counts, and stores the top "top_category_count" many categories for each user
	in a mongo collection (specified here by "collection_category").

	:param top_page_count: Number of top Wikipedia pages to consider.
	:param top_category_count: Number of top categories to return.

	:return: NA
	'''
	client = access_mongo_database(mongo_api)
	collection_wiki = access_mongo_collection(client, wiki_db, wiki_collection)

	collection_tweet = access_mongo_collection(client, topics_db, tweets_collection)
	collection_keyword = access_mongo_collection(client, topics_db, keywords_collection)
	collection_category = access_mongo_collection(client, topics_db, categories_collection)

	es_client = access_elastic_database(elastic_api)

	m = 0
	batch_size = 5
	condition = True

	print 'Inserting into the Mongo collection'
	while condition:
		# get a batch of "batch_size" many users in each iteration from the mongo collection "collection_keyword"
		cursor = collection_keyword.find().skip(m * batch_size).limit(batch_size)
		if cursor.count(with_limit_and_skip=True) < batch_size:
			condition = False

		m = m + 1
		# if m >= 1:
		# 	condition = False

		values = []
		# go through each user in the current batch
		for entry in cursor:
			# get the user details
			actorId = entry['actorId']
			actorName = entry['actorName']
			actorScreenName = entry['actorScreenName']
			# get a list containing a list of keywords for each tweet of the user
			tweets_user = isplit(collection_tweet.find_one({'actorId': actorId})['keyWords'], '#')
			# get the top keywords for the user along with their TfIdf scores
			keywords_user = collection_keyword.find_one({'actorId': actorId})['top_keywords']

			# compute the categories from the tweets and the top keywords of the user and return the top "top_category_count" many categories for the user
			[categories, categories_dict, top_categories] = update_top_categories_for_user(es_client, collection_wiki, tweets_user, keywords_user, top_page_count, top_category_count)
			# values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'categories': categories, 'categoriesFrequency': categories_dict, 'topCategories': top_categories})
			values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'topCategories': top_categories})

		# store the relevant information in the mongo collection "collection_category" for all the users in the current batch
		if values:
			try:
				collection_category.insert_many(values)
			except BulkWriteError as bwe:
				pprint(bwe.details)

	print 'Indexing the Mongo collection'
	collection_category.create_index([('actorId', 1)])
	collection_category.create_index([('actorScreenName', 1)])

	client.close()



if __name__ == '__main__':
	top_page_count = topPageCount
	top_category_count = topCategoryCount

	start = (int)(time.time() * 1000)
	#update_top_categories_for_all_users(top_page_count, top_category_count)
	update_top_categories_for_all_users_parallel(top_page_count, top_category_count)
	stop = (int)(time.time() * 1000)
	print (stop - start)/1000.0