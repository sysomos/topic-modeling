# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'

import time
import csv
from pprint import pprint
from pymongo import MongoClient
from pymongo.errors import BulkWriteError


mongo_api = 'mongodb://10.13.13.32:27017'

users_db = 'twitter_users'
users_collection = 'users'


def return_int_value(d):
	if d.isdigit():
		return int(d)
	return -1


def access_mongo_collection(client, database, collection):
	return client[database][collection]


def access_mongo_database(url=''):
	if url:
		client = MongoClient(url)
	else:
		client = MongoClient()
	return client


def update_user_profile_in_mongo(file, collection_label):
	print 'Updating the Mongo collection'
	with open(file, 'rb') as csvfile:
		reader = csv.reader((line.replace('\0', '') for line in csvfile), delimiter='\t', quotechar=str(u'\u001F'))
		count = 0
		if reader:
			for row in reader:
				for (i, v) in enumerate(row):
					if i == 1:
						actorId = 'TT' + v.decode('utf-8', 'ignore')
					if i == 2:
						actorName = v.decode('utf-8', 'ignore')
					elif i == 3:
						actorScreenName = v.decode('utf-8', 'ignore')
					elif i == 8:
						followersCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 9:
						friendsCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 10:
						listedCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 11:
						createdAt = v.decode('utf-8', 'ignore')
					elif i == 12:
						favouritesCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 16:
						verified = v.decode('utf-8', 'ignore')
					elif i == 17:
						statusesCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 19:
						contributorsEnabled = v.decode('utf-8', 'ignore')
					elif i == 29:
						followRequestSent = v.decode('utf-8', 'ignore')
						if collection_label.find_one({'actorScreenName': actorScreenName}):
							collection_label.update({'actorScreenName': actorScreenName}, {'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'followersCount': followersCount, \
																							'friendsCount': friendsCount, 'listedCount': listedCount, 'createdAt': createdAt, 'favouritesCount': favouritesCount, \
																							'verified': verified, 'statusesCount': statusesCount, 'contributorsEnabled': contributorsEnabled, \
																							'followRequestSent': followRequestSent})
						else:
							collection_label.insert_one({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'followersCount': followersCount, 'friendsCount': friendsCount, \
										'listedCount': listedCount, 'createdAt': createdAt, 'favouritesCount': favouritesCount, 'verified': verified, 'statusesCount': statusesCount, \
										'contributorsEnabled': contributorsEnabled, 'followRequestSent': followRequestSent})
						if count % 10000 == 0:
							print count
						count = count + 1


def create_user_profile_in_mongo(file, collection_label):
	print 'Inserting into the Mongo collection'
	with open(file, 'rb') as csvfile:
		reader = csv.reader((line.replace('\0', '') for line in csvfile), delimiter='\t', quotechar=str(u'\u001F'))
		count = 0
		values = []
		if reader:
			for row in reader:
				for (i, v) in enumerate(row):
					if i == 1:
						actorId = 'TT' + v.decode('utf-8', 'ignore')
					if i == 2:
						actorName = v.decode('utf-8', 'ignore')
					elif i == 3:
						actorScreenName = v.decode('utf-8', 'ignore')
					elif i == 8:
						followersCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 9:
						friendsCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 10:
						listedCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 11:
						createdAt = v.decode('utf-8', 'ignore')
					elif i == 12:
						favouritesCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 16:
						verified = v.decode('utf-8', 'ignore')
					elif i == 17:
						statusesCount = return_int_value(v.decode('utf-8', 'ignore'))
					elif i == 19:
						contributorsEnabled = v.decode('utf-8', 'ignore')
					elif i == 29:
						followRequestSent = v.decode('utf-8', 'ignore')
						values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'followersCount': followersCount, 'friendsCount': friendsCount, \
										'listedCount': listedCount, 'createdAt': createdAt, 'favouritesCount': favouritesCount, 'verified': verified, 'statusesCount': statusesCount, \
										'contributorsEnabled': contributorsEnabled, 'followRequestSent': followRequestSent})
						count = count + 1
						if count % 10000 == 0:
							try:
								collection_label.insert_many(values)
							except BulkWriteError as bwe:
								pprint(bwe.details)
							values = []
						break
		if values:
			try:
				collection_label.insert_many(values)
			except BulkWriteError as bwe:
				pprint(bwe.details)

	print 'Indexing the Mongo Collection'
	collection_label.create_index([('actorId', 1)])
	collection_label.create_index([('actorScreenName', 1)])
	collection_label.create_index([('followersCount', 1)])
	collection_label.create_index([('friendsCount', 1)])
	collection_label.create_index([('listedCount', 1)])
	collection_label.create_index([('favouritesCount', 1)])
	collection_label.create_index([('statusesCount', 1)])


def update_user_profile(file):
	client = access_mongo_database(mongo_api)
	collection_label = access_mongo_collection(client, users_db, users_collection)

	update_user_profile_in_mongo(file, collection_label)

	client.close()


def create_user_profile(file):
	client = access_mongo_database(mongo_api)
	collection_label = access_mongo_collection(client, users_db, users_collection)

	create_user_profile_in_mongo(file, collection_label)

	client.close()


if __name__ == '__main__':
	start = (int)(time.time() * 1000)
	create_user_profile('tmp.csv')
	stop = (int)(time.time() * 1000)
	print (stop - start)/1000.0