# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'


import time
import json
import threading
import multiprocessing
from pymongo import MongoClient
from elasticsearch import Elasticsearch


elastic_api = 'http://10.13.13.32:9200'
mongo_api = 'mongodb://10.13.13.32:27017'

image_db = 'syclops'
topics_db = 'topics'

es_doc = 'image_categories'
categories_collection = 'user_top_categories'


topImages = 50



def lookup_user_in_image_elastic(actorId, top_images=topImages):
	'''
	This method takes the actorId of a user, looks up the user's top categories from a mongo database, does an elastic
	search of those categories in the elastic database of the categories of images, finds the top images that match the
	query and returns the top "top_images" many of them.

	:param actorId: Unique Twitter ID of the user.
	:param top_images: Number of top_images to return.

	:return: A json object containing the user details and a list of the suggested images along with their elastic search scores.
	'''
	client = access_mongo_database(mongo_api)
	collection_category = access_mongo_collection(client, topics_db, categories_collection)

	es_client = access_elastic_database(elastic_api)

	# get the user details
	search_obj = collection_category.find_one({'actorId': actorId})
	actorName = search_obj['actorName']
	actorScreenName = search_obj['actorScreenName']
	# get the user top categories
	categories = search_obj['topCategories']
	# reduce the size of the query if it grows too big as elastic search does not allow a large query
	categories = ','.join(categories[:min(500, len(categories))])
	# search the images elastic database for the user categories
	res = es_client.search(index=image_db, doc_type=es_doc, body={'query': {'match': {'topCategories': {'type': 'boolean', 'query': categories, 'analyzer': 'comma'}}}})

	count = 0
	values_tmp = []
	for hit in res['hits']['hits']:
		if count >= top_images:
			break
		count += 1
		# collect the image labels and the elastic search scores for the top "top_images" many images
		values_tmp.append({'imageLabels': hit['_source']['labels'], 'imageScore': hit['_score']})

	# store the user details and the image details for the top images for that user
	values = []
	values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'suggestedImages': json.dumps(values_tmp)})

	client.close()
	# convert the result to a json object and return it
	return json.dumps(values)


def lookup_user_in_image_elastic_parallel(actorId, top_images, queue):
	'''
	This is a wrapper function used in the parallel version of this program to run the method "lookup_user_in_image_elastic"
	for one user in a given thread and collect the results in a queue.

	:param actorId: Twitter ID of the user.
	:param top_images: Number of top images to return for the user.
	:param queue: A queue to collect the results from all threads.

	:return: NA
	'''
	values = json.loads(lookup_user_in_image_elastic(actorId, top_images))[0]
	queue.put([values['actorId'], values['actorName'], values['actorScreenName'], values['suggestedImages']])


def lookup_users_in_image_elastic_parallel(actorIds, top_images=topImages):
	'''
	This method runs "lookup_user_in_image_elastic" for each user in the given list of users, and merges the json
	output of each call into one big json and returns it. This is a parallel version of "lookup_users_in_image_elastic",
	and runs a parallel thread for each user.

	:param actorIds: A list of Twitter Ids for a set of users.

	:return: A json object containing suggested images for each user.
	'''
	# define a queue required for collecting results of all threads
	manager = multiprocessing.Manager()
	queue = manager.Queue()

	threads = []
	for actorId in actorIds:
		threads.append(threading.Thread(target=lookup_user_in_image_elastic_parallel, args=(actorId, top_images, queue)))

	# start all the threads
	for i in xrange(len(threads)):
		threads[i].start()

	# wait for all the threads to finish
	for i in xrange(len(threads)):
		threads[i].join()

	# collect the results of all the threads together in the list "values"
	values = []
	while not queue.empty():
		entry = queue.get()
		actorId = entry[0]
		actorName = entry[1]
		actorScreenName = entry[2]
		suggestedImages = entry[3]
		values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'suggestedImages': suggestedImages})

	return json.dumps(values)


def lookup_users_in_image_elastic(actorIds, top_images=topImages):
	'''
	This method runs "lookup_user_in_image_elastic" for each user in the given list of users, and merges the json
	output of each call into one big json and returns it.

	:param actorIds: A list of Twitter Ids for a set of users.

	:return: A json object containing suggested images for each user.
	'''
	finalResult = []
	for actorId in actorIds:
		result = lookup_user_in_image_elastic(actorId, top_images)
		finalResult.extend(json.loads(result))
	return json.dumps(finalResult)


def access_elastic_database(url=''):
	'''
	This method returns access to an elastic index.

	:param url: The url of the elastic index.

	:return: An access client to the elastic index at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	es_client = Elasticsearch(url)
	return es_client


def access_mongo_collection(client, database, collection):
	'''
	This method returns access to a mongo collection.

	:param client: An access client to a mongo database.
	:param database: Name of the mongo database.
	:param collection: Name of the mongo collection.

	:return: An access client to the specified mongo collection in the specified mongo database.
	'''
	return client[database][collection]


def access_mongo_database(url=''):
	'''
	This method returns a mongo client.

	:param url: The url of the mongo database.

	:return: An access client to the mongo database at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	if url:
		client = MongoClient(url)
	else:
		client = MongoClient()
	return client


def match_users_to_images(top_images=topImages):
	'''
	This method creates connection to the users database on mongo and the images database on elastic, processes a batch
	of users in each iteration, and computes the top images for each user in that batch by doing an elastic search of the
	top categories of the user among the categories of the images, and finally returns the top "top_images" many of them.

	:param top_images: Number of top images to return for each user.

	:return: A json object containing top images for each user.
	'''
	client = access_mongo_database(mongo_api)
	collection_category = access_mongo_collection(client, topics_db, categories_collection)

	m = 0
	batch_size = 50
	condition = True

	values = []
	while condition:
		# get a batch of "batch_size" many users in each iteration from the mongo collection "collection_keyword"
		cursor = collection_category.find().skip(m * batch_size).limit(batch_size)
		if cursor.count(with_limit_and_skip=True) < batch_size:
			condition = False

		m = m + 1
		# if m >= 2:
		# 	condition = False

		# get the user IDs for the current batch of users
		actorIds = [entry['actorId'] for entry in cursor]

		result = json.loads(lookup_users_in_image_elastic_parallel(actorIds, top_images))
		values.extend(result)

	return json.dumps(values)



if __name__ == '__main__':
	top_images = 10

	start = (int)(time.time() * 1000)
	res = match_users_to_images(top_images)
	res = json.loads(res)
	print res
	stop = (int)(time.time() * 1000)
	print (stop - start) / 1000.0