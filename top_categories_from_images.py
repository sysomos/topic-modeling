# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'


'''
	This program reads the labels for all images from a specified mongo database, performs an elastic search of each
	label in Wikipedia with the remaining labels forming a context, normalizes the elastic scores of the Wikipedia pages
	to a score between 0 and 1 so that they can be compared across several queries, sorts the Wikipedia pages by their
	normalized scores and gets the top M Wikipedia pages for the user, collects all the categories of these pages and
	all their ancestors, sorts the categories by their frequency counts, and finally stores the top N categories for
	each user as one document in a specified mongo collection in a specified mongo database.
'''


import time
import itertools
import threading
import multiprocessing
from pprint import pprint
from pymongo import MongoClient
from collections import defaultdict
from elasticsearch import Elasticsearch
from pymongo.errors import BulkWriteError
from pymongo import UpdateOne


big_api = 'mongodb://10.12.40.68:27017'
small_api = 'mongodb://52.27.206.210:27017'
elastic_api = 'http://10.13.13.32:9200'
wiki_api = 'mongodb://10.13.13.32:27017'

image_db = 'syclops'
wiki_db = 'wiki20151002'

ancestor_collection = 'categories_ancestors'
image_collection = 'top_images'
categories_collection = 'image_top_categories'
es_doc = 'image_categories'


topPageCount = 30
topCategoryCount = 100



def categories_to_topcategories(categories, top_category_count=topCategoryCount):
	'''
	This method takes a list of categories, sorts them by their frequency count, and returns the top "top_category_count"
	many categories.

	:param categories: A list of categories.
	:param top_category_count: Number of top categories to return.

	:return: A list containing a dictionary of the frequency counts of the categories and the top categories of a given user.
	'''
	categories_dict = defaultdict(int)
	for category in categories:
		categories_dict[category] += 1
	sorted_dict = sorted(categories_dict, key=categories_dict.get, reverse=True)
	top_categories = sorted_dict[:top_category_count]
	return [categories_dict, top_categories]


def pages_to_categories(collection_wiki, pagesSorted):
	'''
	This method takes a list of Wikipedia pages and returns a list of all ancestors of their categories.

	:param collection_wiki: Access client to a mongo collection that stores the category hierarchy of Wikipedia.
	:param pagesSorted: A list of Wikipedia pages.

	:return: A list of all ancestors of all categories occurring in any of the Wikipedia pages in "pagesSorted".
	'''
	# collect all the categories occurring in any of the Wikipedia pages in "pagesSorted"
	categories = [page['_source']['category'] for page in pagesSorted]
	combined_categories = list(itertools.chain(*categories))
	combined_categories = [category.lower().replace('.', '_') for category in combined_categories]

	# find all ancestors of all the categories obtained above from the mongo collection "collection_wiki"
	fd = list(collection_wiki.find({'category': {'$in': combined_categories}}))

	cat = []
	[cat.extend(entry['ancestors_list']) for entry in fd]

	return cat


def normalize_scores_of_wikipages(pages):
	'''
	This method computes a normalized score between 0 and 1 for a given list of Wikipedia pages and their elastic search scores.

	:param pages: A list of Wikipedia pages along with their elastic search scores.

	:return: A list of Wikipedia pages with updated scores.
	'''
	sum = 0
	for page in pages:
		sum = sum + page['_score']
	for page in pages:
		page['_score'] = page['_score'] / sum
	return pages


def get_top_pages_for_image(es_client, labels_image, top_page_count=topPageCount):
	'''
	This method computes the top Wikipedia pages for a given image. It does an elastic search query of each label of the
	image on Wikipedia using the remaining labels of the image as a context. Then it normalizes the elastic search score
	of each Wikipedia page to a score between 0 and 1. Finally, this method sorts the Wikipedia pages using the
	normalized scores and returns the top "top_page_count" many Wikipedia pages for the given image.

	:param es_client: Access client to elastic search.
	:param labels_image: A list of labels for a given image.
	:param top_page_count: Number of top Wikipedia pages to return.

	:return: A list of Wikipedia pages.
	'''
	pages_and_normalizedscores = []
	# go through each label of the image
	for entry in labels_image:
		# set the label as a keyword
		keyword = entry
		# set the remaining labels as a context
		context = [word for word in labels_image if word != keyword]
		# reduce the size of the context if it grows too big as elastic search does not allow a large context
		context = ','.join(context[:min(500, len(context))])
		# search the Wikipedia elastic database for the keyword and the context
		res = es_client.search(index=wiki_db,
								body={
									'query': {
										'bool': {
											'must': {'match': {'page.title': keyword}},
											'should': {
												'multi_match': {
													'query': context,
													'type': 'most_fields',
													'fields': ['page.title', 'page.text'],
													'minimum_should_match': '50%'
												}
											}
										}
									}
								})
		# from the results of the elastic search, discard pages that are redirects
		pages = [hit for hit in res['hits']['hits'] if hit['_source']['category'] and not hit['_source']['redirect']]
		if len(pages) > 0:
			# normalize the scores of the pages to a number between 0 and 1 for comparison with the results of other queries
			normalizedPages = normalize_scores_of_wikipages(pages)
			# store the pages along with their normalized scores in the list "pages_and_normalizedscores"
			pages_and_normalizedscores.extend(normalizedPages)

	# sort the Wikipedia pages by their scores and return the top "top_page_count" many of them
	pagesSorted = sorted(pages_and_normalizedscores, key=lambda x: x['_score'], reverse=True)
	return pagesSorted[:top_page_count]


def update_top_categories_for_image_parallel(es_client, collection_wiki, id, imageId, imageUrl, labels_image, queue, top_page_count=topPageCount, top_category_count=topCategoryCount):
	'''
	This is a wrapper function to compute the top Wikipedia pages and the top categories for a given image in a parallel thread and collect the results in a queue.
	'''
	pagesSorted = get_top_pages_for_image(es_client, labels_image, top_page_count)
	categories = pages_to_categories(collection_wiki, pagesSorted)
	[categories_dict, top_categories] = categories_to_topcategories(categories, top_category_count)
	queue.put([id, imageId, imageUrl, labels_image, categories, categories_dict, top_categories])
	return categories


def update_top_categories_for_image(es_client, collection_wiki, labels_image, top_page_count=topPageCount, top_category_count=topCategoryCount):
	'''
	This is a wrapper function to compute the top Wikipedia pages and the top categories for a given image.
	'''
	pagesSorted = get_top_pages_for_image(es_client, labels_image, top_page_count)
	categories = pages_to_categories(collection_wiki, pagesSorted)
	[categories_dict, top_categories] = categories_to_topcategories(categories, top_category_count)
	return [categories, categories_dict, top_categories]


def access_elastic_database(index, doc, url=''):
	'''
	This method sets up the elastic database for storing all categories and the top categories of all images.

	:param index: Name of the elastic index.
	:param doc: Name of the elastic document.
	:param url: Url of the elastic index.

	:return: Access client to the elastic index.
	'''
	es_client = Elasticsearch(url)
	request_body = {
		'settings': {
			'index': {
				'analysis': {
					'analyzer': {
						'comma': {
							'type': 'pattern',
							'pattern': ','
						}
					}
				}
			}
		},
		'mappings': {
			doc: {
				'properties': {
					'imageId': {'type': 'string', 'index': 'not_analyzed'},
					'imageUrl': {'type': 'string', 'index': 'not_analyzed'},
					'labels': {'type': 'string', 'analyzer': 'comma'},
					'categories': {'type': 'string', 'analyzer': 'comma'},
					'topCategories': {'type': 'string', 'analyzer': 'comma'}
				}
			}
		}
	}
	es_client.indices.create(index=index, body=request_body, ignore=400)
	return es_client


def access_mongo_collection(client, database, collection):
	'''
	This method returns access to a mongo collection.

	:param client: An access client to a mongo database.
	:param database: Name of the mongo database.
	:param collection: Name of the mongo collection.

	:return: An access client to the specified mongo collection in the specified mongo database.
	'''
	return client[database][collection]


def access_mongo_database(url=''):
	'''
	This method returns a mongo client.

	:param url: The url of the mongo database.

	:return: An access client to the mongo database at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	if url:
		client = MongoClient(url)
	else:
		client = MongoClient()
	return client


def update_top_categories_for_all_images_in_parallel(top_page_count=topPageCount, top_category_count=topCategoryCount):
	'''
	This method collects the labels of each image, performs an elastic search of each label on Wikipedia using the
	remaining labels as a context, normalizes the scores of the Wikipedia pages to a score between 0 and 1, sorts the
	Wikipedia pages by the normalized score and gets the top "top_page_count" many Wikipedia pages for the image,
	collects all the categories of these pages and all their ancestors, sorts the categories by their frequency counts,
	and stores the top "top_category_count" many categories for each image in a mongo collection (specified here by
	"collection_category"). This is a parallel implementation of the method "update_top_categories_for_all_users". Here
	the users are processed in batches, and in each batch a new thread is created for each user so that the processing
	of all the users in the current batch can be done in parallel.

	:param top_page_count: Number of top Wikipedia pages to consider.
	:param top_category_count: Number of top categories to return.

	:return: NA
	'''
	client_small = access_mongo_database(small_api)
	collection_label = access_mongo_collection(client_small, image_db, image_collection)

	client_wiki = access_mongo_database(wiki_api)
	collection_wiki = access_mongo_collection(client_wiki, wiki_db, ancestor_collection)

	client = access_mongo_database()
	collection_category = access_mongo_collection(client, image_db, categories_collection)

	es_client = access_elastic_database(image_db, es_doc, elastic_api)

	# define a queue required for collecting results of all threads
	manager = multiprocessing.Manager()
	queue = manager.Queue()

	id = 1
	m = 0
	batch_size = 20
	condition = True

	print 'Inserting into the Mongo collection'
	while condition:
		# get a batch of "batch_size" many images in each iteration from the mongo collection "collection_label"
		cursor = collection_label.find().skip(m * batch_size).limit(batch_size)
		if cursor.count(with_limit_and_skip=True) < batch_size:
			condition = False

		m = m + 1
		# if m >= 1:
		# 	condition = False

		threads = []
		# go through each image in the current batch
		for entry in cursor:
			if entry['labels'] and entry['labels']['sysomos']:
				# get the image details
				imageId = entry['_id']
				imageUrl = entry['image_url']
				labels_image = entry['labels']['sysomos']['inferred_labels']
				if labels_image:
					# create a new thread for the image that computes the relevant categories and returns the top "top_category_count" many categories for the image
					threads.append(threading.Thread(target=update_top_categories_for_image_parallel, \
													args=(es_client, collection_wiki, id, imageId, imageUrl, labels_image, queue, top_page_count, top_category_count)))
					id = id + 1

		# start all the threads
		for i in xrange(len(threads)):
			threads[i].start()

		# wait for all the threads to finish
		for i in xrange(len(threads)):
			threads[i].join()

		# collect the results of all the threads together in the list "values"
		values = []
		while not queue.empty():
			entry = queue.get()
			i = entry[0]
			imageId = entry[1]
			imageUrl = entry[2]
			labels_image = entry[3]
			categories = entry[4]
			categories_dict = entry[5]
			top_categories = entry[6]
			values.append(UpdateOne({'imageId': imageId}, {'$set': {'imageId': imageId, 'imageUrl': imageUrl, 'labels': labels_image, \
																	'categories': categories, 'categoriesFrequency': categories_dict, 'topCategories': top_categories}}, upsert=True))
			doc = {'imageId': imageId, 'imageUrl': imageUrl, 'labels': ','.join(labels_image), 'categories': ','.join(categories), 'topCategories': ','.join(top_categories)}
			es_client.index(index=image_db, doc_type=es_doc, id=id, body=doc)

		# store the relevant information in the mongo collection "collection_category" for all the images in the current batch
		try:
			collection_category.bulk_write(values)
		except BulkWriteError as bwe:
			pprint(bwe.details)

	print 'Indexing the Mongo collection'
	collection_category.create_index([('imageId', 1)])

	client_small.close()
	client_wiki.close()
	client.close()


def update_top_categories_for_all_images(top_page_count=topPageCount, top_category_count=topCategoryCount):
	'''
	This method collects the labels of each image, performs an elastic search of each label on Wikipedia using the
	remaining labels as a context, normalizes the scores of the Wikipedia pages to a score between 0 and 1, sorts the
	Wikipedia pages by the normalized score and gets the top "top_page_count" many Wikipedia pages for the image,
	collects all the categories of these pages and all their ancestors, sorts the categories by their frequency counts,
	and stores the top "top_category_count" many categories for each image in a mongo collection (specified here by
	"collection_category").

	:param top_page_count: Number of top Wikipedia pages to consider.
	:param top_category_count: Number of top categories to return.

	:return: NA
	'''
	client_small = access_mongo_database(small_api)
	collection_label = access_mongo_collection(client_small, image_db, image_collection)

	client_wiki = access_mongo_database(wiki_api)
	collection_wiki = access_mongo_collection(client_wiki, wiki_db, ancestor_collection)

	client = access_mongo_database()
	collection_category = access_mongo_collection(client, image_db, categories_collection)

	es_client = access_elastic_database(image_db, es_doc, elastic_api)

	id = 1
	m = 0
	batch_size = 5
	condition = True

	print 'Inserting into the Mongo collection'
	while condition:
		# get a batch of "batch_size" many images in each iteration from the mongo collection "collection_label"
		cursor = collection_label.find().skip(m * batch_size).limit(batch_size)
		if cursor.count(with_limit_and_skip=True) < batch_size:
			condition = False

		m = m + 1
		# if m >= 1:
		# 	condition = False

		values = []
		# go through each image in the current batch
		for entry in cursor:
			if entry['labels'] and entry['labels']['sysomos']:
				# get the image details
				imageId = entry['_id']
				imageUrl = entry['image_url']
				labels_image = entry['labels']['sysomos']['inferred_labels']
				if labels_image:
					# compute the categories from the labels of the image and return the top "top_category_count" many categories for the image
					[categories, categories_dict, top_categories] = update_top_categories_for_image(es_client, collection_wiki, labels_image, top_page_count, top_category_count)
					values.append(UpdateOne({'imageId': imageId}, {'$set': {'imageId': imageId, 'imageUrl': imageUrl, 'labels': labels_image, \
																			'categories': categories, 'categoriesFrequency': categories_dict, 'topCategories': top_categories}}, upsert=True))
					doc = {'imageId': imageId, 'imageUrl': imageUrl, 'labels': ','.join(labels_image), 'categories': ','.join(categories), 'topCategories': ','.join(top_categories)}
					es_client.index(index=image_db, doc_type=es_doc, id=id, body=doc)
					id = id + 1
					print id

		# store the relevant information in the mongo collection "collection_category" for all the images in the current batch
		try:
			collection_category.bulk_write(values)
		except BulkWriteError as bwe:
			pprint(bwe.details)

	print 'Indexing the Mongo collection'
	collection_category.create_index([('imageId', 1)])

	client_small.close()
	client_wiki.close()
	client.close()



if __name__ == '__main__':
	top_page_count = topPageCount
	top_category_count = topCategoryCount

	start = (int)(time.time() * 1000)
	#update_top_categories_for_all_images(top_page_count, top_category_count)
	update_top_categories_for_all_images_in_parallel(top_page_count, top_category_count)
	stop = (int)(time.time() * 1000)
	print (stop - start)/1000.0