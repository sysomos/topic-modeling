#!/flask/bin/python
__author__ = 'Koushik Pal'


'''
	This program takes a string of comma-separated labels and an integer N as inputs and returns the top N categories
	that match those labels. The program splits the string into labels, and performs an elastic search of each label in
	Wikipedia with the remaining labels forming a context. The elastic scores of the Wikipedia pages are normalized to
	a score between 0 and 1 so that they can be compared across several queries. Then the Wikipedia pages are sorted by
	their scores, and all the categories of the top M pages along with all their ancestors are computed. The categories
	are sorted by their frequencies and finally, the top N categories are returned as a json. This rest service supports
	only GET operation.
'''


import itertools
from pymongo import MongoClient
from collections import defaultdict
from elasticsearch import Elasticsearch
from flask import Flask, jsonify, make_response
app = Flask(__name__)


elastic_api = 'http://10.13.13.32:9200'
wiki_api = 'mongodb://10.13.13.32:27017'

wiki_db = 'wiki20151002'


topPageCount = 30
topCategoryCount = 100



def categories_to_topcategories(categories, top_category_count=topCategoryCount):
	'''
	This method takes a list of categories, sorts them by their frequency count, and returns the top "top_category_count"
	many categories.

	:param categories: A list of categories.
	:param top_category_count: Number of top categories to return.

	:return: A list containing a dictionary of the frequency counts of the categories and the top categories.
	'''
	# create a dictionary of frequency counts of the categories in "categories"
	categories_dict = defaultdict(int)
	for category in categories:
		categories_dict[category] += 1
	# sort the categories by their frequency counts and return the top "top_category_count" many of them
	sorted_dict = sorted(categories_dict, key=categories_dict.get, reverse=True)
	top_categories = sorted_dict[:top_category_count]
	# return the top categories and the frequency counts of all categories
	return [categories_dict, top_categories]


def pages_to_categories(collection_wiki, pagesSorted):
	'''
	This method takes a list of Wikipedia pages and returns a list of all ancestors of their categories.

	:param collection_wiki: Access client to a mongo collection that stores the category hierarchy of Wikipedia.
	:param pagesSorted: A list of Wikipedia pages.

	:return: A list of all ancestors of all categories occurring in any of the Wikipedia pages in "pagesSorted".
	'''
	# collect all the categories occurring in any of the Wikipedia pages in "pagesSorted"
	categories = [page['_source']['category'] for page in pagesSorted]
	combined_categories = list(itertools.chain(*categories))
	combined_categories = [category.lower().replace('.', '_') for category in combined_categories]

	# find all ancestors of all the categories obtained above from the mongo collection "collection_wiki"
	fd = list(collection_wiki.find({'category': {'$in': combined_categories}}))

	cat = []
	[cat.extend(entry['ancestors_list']) for entry in fd]

	return cat


def normalize_scores_of_wikipages(pages):
	'''
	This method computes a normalized score between 0 and 1 for a given list of Wikipedia pages and their elastic search scores.

	:param pages: A list of Wikipedia pages along with their elastic search scores.

	:return: A list of Wikipedia pages with updated scores.
	'''
	sum = 0
	for page in pages:
		sum = sum + page['_score']
	for page in pages:
		page['_score'] = page['_score'] / sum
	return pages


def access_elastic_database(url=''):
	'''
	This method returns access to an elastic index.

	:param url: The url of the elastic index.

	:return: An access client to the elastic index at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	es_client = Elasticsearch(url)
	return es_client


def access_mongo_collection(client, database, collection):
	'''
	This method returns access to a mongo collection.

	:param client: An access client to a mongo database.
	:param database: Name of the mongo database.
	:param collection: Name of the mongo collection.

	:return: An access client to the specified mongo collection in the specified mongo database.
	'''
	return client[database][collection]


def access_mongo_database(url=''):
	'''
	This method returns a mongo client.

	:param url: The url of the mongo database.

	:return: An access client to the mongo database at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	if url:
		client = MongoClient(url)
	else:
		client = MongoClient()
	return client


def get_top_pages_for_labels(es_client, labels, top_page_count=topPageCount):
	'''
	This method computes the top Wikipedia pages for a given set of labels. It does an elastic search query of each top
	label on Wikipedia using the remaining labels as a context. Then it normalizes the scores to a number between 0 and 1.
	Finally, it sorts the Wikipedia pages using the new scores and returns the top "top_page_count" many Wikipedia pages.

	:param es_client: Access client to elastic search.
	:param labels: A list of labels.
	:param top_page_count: Number of top Wikipedia pages to return.

	:return: A list of Wikipedia pages.
	'''
	pages_and_normalizedscores = []
	# go through each label in "labels"
	for entry in labels:
		# get the keyword
		keyword = entry
		# use the remaining labels as a context
		context = [word for word in labels if word != keyword]
		# reduce the size of the context if it grows too big as elastic search does not allow a large context
		context = ','.join(context[:min(500, len(context))])
		# search the Wikipedia elastic database for the keyword and the context
		res = es_client.search(index=wiki_db,
								body={
									'query': {
										'bool': {
											'must': {'match': {'page.title': keyword}},
											'should': {
												'multi_match': {
													'query': context,
													'type': 'most_fields',
													'fields': ['page.title', 'page.text'],
													'minimum_should_match': '50%'
												}
											}
										}
									}
								})
		# from the results of the elastic search, discard pages that are redirects
		pages = [hit for hit in res['hits']['hits'] if hit['_source']['category'] and not hit['_source']['redirect']]
		if len(pages) > 0:
			# normalize the scores of the pages to a number between 0 and 1 for comparison with the results of other queries
			normalizedPages = normalize_scores_of_wikipages(pages)
			# store the pages along with their new scores in the list "pages_and_normalizedscores"
			pages_and_normalizedscores.extend(normalizedPages)

	# sort the Wikipedia pages by their scores and return the top "top_page_count" many of them
	pagesSorted = sorted(pages_and_normalizedscores, key=lambda x: x['_score'], reverse=True)
	return pagesSorted[:top_page_count]


def update_top_categories_for_labels(labels, top_page_count=topPageCount, top_category_count=topCategoryCount):
	'''
	This method computes the top categories for the given list of labels.

	:param labels: A list of labels (strings).
	:param top_page_count: Number of top Wikipedia pages to consider.
	:param top_category_count: Number of top categories to return.

	:return:  A list containing a dictionary of the frequency counts of the categories and the top categories for the given list of labels.
	'''
	es_client = access_elastic_database(elastic_api)
	client_wiki = access_mongo_database(wiki_api)
	collection_wiki = access_mongo_collection(client_wiki, wiki_db, 'categories_ancestors')

	pagesSorted = get_top_pages_for_labels(es_client, labels, top_page_count)
	categories = pages_to_categories(collection_wiki, pagesSorted)
	[categories_dict, top_categories] = categories_to_topcategories(categories, top_category_count)
	return [categories_dict, top_categories]


@app.route('/api/v1.0/topcategories/<string:labels>/<int:top_category_count>', methods=['GET'])
def get_top_categories(labels, top_category_count):
	'''
	This method sets up a rest service that takes a string of comma-separated labels and an integer as inputs and
	returns a json output of their top categories.

	:param labels: A string of comma-separated labels/words.
	:param top_category_count: Number of top categories to return.

	:return: A json object containing the labels and their top categories.
	'''
	labels = labels.strip('\"').split(',')
	[categories_dict, top_categories] = update_top_categories_for_labels(labels, top_category_count=top_category_count)
	return jsonify({'labels': labels, 'topCategories': top_categories})


@app.errorhandler(404)
def not_found(error):
	'''
	This method handles "Not Found" or 404 error.

	:param error: Error encountered.

	:return: "Not Found or 404 error.
	'''
	return make_response(jsonify({'error': 'Not found'}), 404)



if __name__ == "__main__":
	app.run(debug=True)