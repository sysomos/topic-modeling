# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'


import sys
import time
import json
import nltk
import threading
import multiprocessing
from pprint import pprint
from pymongo import MongoClient
from elasticsearch import Elasticsearch
from pymongo.errors import BulkWriteError


import create_mongodb_from_solr
import top_keywords_from_tweets
import top_categories_from_tweets


elastic_api = 'http://10.13.13.32:9200'
wiki_api = 'mongodb://10.13.13.32:27017'
wiki_db = 'wiki20151002'


lock = threading.Lock()


def update_top_categories_for_user_parallel(es_client, collection_wiki, id, link, score, text, text_user, keywords_user, top_page_count, top_category_count, queue):
	pagesSorted = top_categories_from_tweets.get_top_pages_for_user(es_client, text_user, keywords_user, top_page_count)
	categories = top_categories_from_tweets.pages_to_categories(collection_wiki, pagesSorted)
	[categories_dict, top_categories] = top_categories_from_tweets.categories_to_topcategories(categories, top_category_count)
	queue.put([id, link, score, text, categories, categories_dict, top_categories])


def access_elastic_database(index, url = ''):
	es_client = Elasticsearch(url)
	es_client.indices.create(index = index, ignore = 400)
	return es_client


def access_mongo_collection(client, database, collection):
	return client[database][collection]


def access_mongo_database(url = ''):
	if url:
		client = MongoClient(url)
	else:
		client = MongoClient()
	return client


def main(file, top_keyword_count, top_page_count, top_category_count):
	stopWords = [line.lower().split() for line in open('stopwords.english')]

	client = access_mongo_database()
	collection_text = access_mongo_collection(client, 'test', 'user_text')
	collection_keyword = access_mongo_collection(client, 'test', 'user_top_keywords')
	collection_category = access_mongo_collection(client, 'test', 'user_top_categories')

	collection_text.create_index([('id', 1)])
	collection_keyword.create_index([('id', 1)])
	collection_category.create_index([('id', 1)])

	client_wiki = access_mongo_database(wiki_api)
	collection_wiki = access_mongo_collection(client_wiki, wiki_db, 'categories_ancestors')

	es_client = access_elastic_database('syclops', elastic_api)

	grammar = r'''
		Nouns:  {<A>*<N>*<\^>+}
				{<A>*<\^>*<N>+}
		Hash:   {<\#>}
	'''
	cp = nltk.RegexpParser(grammar)

	with open(file) as json_file:
		try:
			json_data = json.loads(json_file.read())
		except ValueError, e:
			print 'Input valid json file'
			sys.exit(1)
		json_file.close()

	id = 0
	values = []
	for entry in json_data:
		if id >= 1000:
			break
		text = entry['text']
		if text:
			score = entry['score']
			if score:
				id = id + 1
				link = entry['link']
				text_processed = ' # '.join(text.split('.'))

				textTagged = create_mongodb_from_solr.tokenize_and_tag_cmu(text_processed)
				keyWords = create_mongodb_from_solr.get_keywords(textTagged, cp, stopWords)
				values.append({'id': id, 'link': link, 'score': score, 'keyWords': keyWords, 'text': text})

	print id

	if values:
		try:
			collection_text.insert_many(values)
		except BulkWriteError as bwe:
			pprint(bwe.details)

	[corpus_tfidf, dict_id2token] = top_keywords_from_tweets.compute_tfidf_of_tweets(collection_text)

	values = []
	for entry, doc in zip(collection_text.find(), corpus_tfidf):
		id = entry['id']
		link = entry['link']
		score = entry['score']
		text = entry['text']
		top_keywords = top_keywords_from_tweets.compute_top_keywords_from_tfidf(doc, dict_id2token, top_keyword_count)
		values.append({'id': id, 'link': link, 'score': score, 'top_keywords': top_keywords, 'text': text})

	if values:
		try:
			collection_keyword.insert_many(values)
		except BulkWriteError as bwe:
			pprint(bwe.details)

	manager = multiprocessing.Manager()
	queue = manager.Queue()

	m = 0
	batch_size = 20
	condition = True
	while condition:
		print m
		cursor = collection_keyword.find().skip(m * batch_size).limit(batch_size)
		if cursor.count(with_limit_and_skip = True) < batch_size:
			condition = False

		threads = []
		m = m + 1
		for entry in cursor:
			id = entry['id']
			link = entry['link']
			score = entry['score']
			text = entry['text']
			text_user = top_categories_from_tweets.isplit(collection_text.find_one({'id': id})['keyWords'], '#')
			keywords_user = collection_keyword.find_one({'id': id})['top_keywords']

			threads.append(threading.Thread(target = update_top_categories_for_user_parallel, args = (es_client, collection_wiki, id, link, score, text, \
																										text_user, keywords_user, top_page_count, top_category_count, queue)))

		for i in xrange(len(threads)):
			threads[i].start()

		for i in xrange(len(threads)):
			threads[i].join()

		values = []
		while not queue.empty():
			entry = queue.get()
			id = entry[0]
			link = entry[1]
			score = entry[2]
			text = entry[3]
			categories = entry[4]
			categories_dict = entry[5]
			top_categories = entry[6]
			values.append({'id': id, 'link': link, 'score': score, 'scoreAsString': str(score), 'categories': categories, \
							'categoriesFrequency': categories_dict, 'topCategories': top_categories, 'topCategoriesAsString': ','.join(top_categories), 'text': text})

		if values:
			try:
				collection_category.insert_many(values)
			except BulkWriteError as bwe:
				pprint(bwe.details)

	client.close()
	client_wiki.close()



if __name__ == '__main__':
	top_keyword_count = 100
	top_page_count = 30
	top_category_count = 30
	file = 'parts/output.json'

	start = (int)(time.time() * 1000)
	main(file, top_keyword_count, top_page_count, top_category_count)
	stop = (int)(time.time() * 1000)
	print (stop - start)/1000.0