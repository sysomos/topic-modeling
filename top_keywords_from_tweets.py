# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'


''' This program reads the keywords for given users, combines them into a big corpus, runs TfIdf on it to get the
	important keywords for each user, and finally stores the top N keywords (along with their TfIdf scores) and other
	relevant user details as one document for each user in a specified mongo collection in a specified mongo database.
'''


import logging
from pymongo import MongoClient
from gensim import corpora, models, similarities


logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


mongo_api = 'mongodb://10.13.13.32:27017'

topics_db = 'topics'

tweets_collection = 'user_recent_tweets'
keywords_collection = 'user_top_keywords'


topKeywordCount = 100



def compute_top_keywords_from_tfidf(doc, dict_id2token, top_keyword_count=topKeywordCount):
	'''
	This method sorts the keywords of each user by their TfIdf scores and returns the top "top top_keyword_count" many
	keywords for each user.

	:param doc: A dictionary of all keywords with their TfIdf scores for a given user.
	:param dict_id2token: The inverse map of word2vec.
	:param top_keyword_count: The number of top keywords to return for each user.

	:return: A list of pairs of top keywords along with their TfIdf scores.
	'''
	sortKey = lambda item: -item[1]
	# sort the keywords by their TfIdf scores
	sortedDoc = sorted(doc, key=sortKey)
	# get the top "top_keyword_count" many keywords
	topIds = sortedDoc[:top_keyword_count]
	# convert the keyword Ids into actual keywords using the inverse of the word2vec map
	top_keywords = []
	for id in topIds:
		top_keywords.append((dict_id2token[id[0]], id[1]))
	return top_keywords


def id2token(dictionary):
	'''
	This method takes a dictionary, converts (key, value) pairs into (value, key) pairs, and returns an inverse dictionary.

	:param dictionary: A dictionary of (key, value) pairs

	:return: An inverse dictionary of (value, key) pairs
	'''
	return dict((v, k) for k, v in dictionary.token2id.iteritems())


def compute_tfidf_of_tweets(collection_tweet):
	'''
	This method collects all keywords of all users in the mongo collection "collection_tweet" into a corpus and runs TfIdf on it.

	:param collection_tweet: Name of the mongo collection containing the keywords information for users.

	:return: A list containing the corpus with TfIdf scores and the inverse map of word2vec.
	'''
	# create a dictionary of all keywords of all users in the English language
	dictionary = corpora.Dictionary(user['keyWords'] for user in collection_tweet.find({'language': 'en'}))
	dict_id2token = id2token(dictionary)

	class MyCorpus(object):
		def __iter__(self):
			for entry in collection_tweet.find({'language': 'en'}):
				yield dictionary.doc2bow(entry['keyWords'])

	# create a corpus of all the keywords from the above dictionary
	corpus = MyCorpus()

	# run TfIdf on the above corpus
	tfidf = models.TfidfModel(corpus)
	corpus_tfidf = tfidf[corpus]

	return [corpus_tfidf, dict_id2token]


def access_mongo_collection(client, database, collection):
	'''
	This method returns access to a mongo collection.

	:param client: An access client to a mongo database.
	:param database: Name of the mongo database.
	:param collection: Name of the mongo collection.

	:return: An access client to the specified mongo collection in the specified mongo database.
	'''
	return client[database][collection]


def access_mongo_database(url=''):
	'''
	This method returns a mongo client.

	:param url: The url of the mongo database.

	:return: An access client to the mongo database at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	if url:
		client = MongoClient(url)
	else:
		client = MongoClient()
	return client


def update_top_keywords_for_all_users(top_keyword_count=topKeywordCount):
	'''
	This method collects all the keywords of all users into a corpus, runs TfIdf on it, and finds and stores the top
	top_keyword_count many keywords (along with their TfIdf scores) as one document for each user in a specified mongo collection.

	:param top_keyword_count: Number of top keywords to store for each user.

	:return: NA
	'''
	client = access_mongo_database(mongo_api)
	collection_tweet = access_mongo_collection(client, topics_db, tweets_collection)

	# collect all keywords of all users into a corpus and run TfIdf on it
	[corpus_tfidf, dict_id2token] = compute_tfidf_of_tweets(collection_tweet)

	# get all users who has tweet in the English laguage
	cursor = collection_tweet.find({'language': 'en'})
	values = []

	# for each user sort the keywords by their TfIdf scores and return the top "top_keyword_count" many keywords
	for user, doc in zip(cursor, corpus_tfidf):
		actorId = user['actorId']
		actorName = user['actorName']
		actorScreenName = user['actorScreenName']
		top_keywords = compute_top_keywords_from_tfidf(doc, dict_id2token, top_keyword_count)
		values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'top_keywords': top_keywords})

	# save the top keywords (along with their TfIdf scores) and relevant user details in the mongo collection specified by "collection_keyword"
	if values:
		print 'Inserting into the Mongo collection'
		collection_keyword = access_mongo_collection(client, topics_db, keywords_collection)

		try:
			collection_keyword.insert_many(values)
		except BulkWriteError as bwe:
			pprint(bwe.details)

		print 'Indexing the Mongo collection'
		collection_keyword.create_index([('actorId', 1)])
		collection_keyword.create_index([('actorScreenName', 1)])

	client.close()



if __name__ == '__main__':
	top_keyword_count = 100

	start = (int)(time.time() * 1000)
	update_top_keywords_for_all_users(top_keyword_count)
	stop = (int)(time.time() * 1000)
	print (stop - start)/1000.0