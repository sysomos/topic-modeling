# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'


import re
import json
import urllib


def process_line(line, dict):
	line = urllib.unquote(line).decode('utf-8')
	line = line.replace('Category:', '')
	line = line.split('\t')
	key = line[0]
	value = line[1]
	value = re.sub('^\(', '', value)
	value = re.sub('\),$','', value)
	value = value.split('),(')

	for entry in value:
		val_entry = re.match('.*?([0-9]+)$', entry).group(1)
		key_entry = entry[0:entry.rfind(val_entry)-1].replace('.', '_')
		dict[key_entry] = int(val_entry)

	return key.replace('.', '_')


def process_file(file_path):
	dict = {}

	with open(file_path) as data_file:
		for line in data_file:
			dict_line = {}
			key = process_line(line, dict_line)
			dict[key] = dict_line

	json_data = json.dumps(dict, encoding = 'utf-8')
	return json_data


if __name__ == '__main__':
	json_data = process_file('categories_en.nt_ancestors_raw_1')
	print json_data