# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'


''' This program does solr queries on given users, collects all their recent tweets and retweets, computes their relevant keywords,
	and stores the keywords, hashtags, atmentions and urls along with the user details as one document per user in a specified mongo
	collection in a specified mongo database.
'''


import os
import re
import time
import nltk
import requests
import subprocess
import threading
import multiprocessing
from pprint import pprint
from collections import Counter
from pymongo import MongoClient
from pymongo.errors import BulkWriteError


mongo_api = 'mongodb://10.13.13.32:27017'
solr_url_prod = 'http://10.200.24.9'
solr_url_staging = 'http://dsapi-stg.grid.stg.yyz.corp.pvt:80'

users_db = 'twitter_users'
topics_db = 'topics'

users_collection = 'users'
tweets_collection = 'user_recent_tweets'


# required for using the "cmu" tagger
lock = threading.Lock()



def update_dictionary(entries, dict):
	'''
	This method updates a given dictionary "dict" of counts with the counts of the items in "entries".

	:param entries: A list of items.
	:param dict: A dictionary of counts.

	:return: A dictionary of updated counts.
	'''
	dict_entries = Counter(entries)
	for entry, value in dict_entries.iteritems():
		if dict.get(entry):
			dict.update({entry: dict.get(entry) + value})
		else:
			dict.update({entry: value})
	return dict


def update_database_one_user(collection_tweet, actorId, actorName, actorScreenName, tweetIds, hashTags, atMentions, urls, keyWords):
	'''
	This method updates the relevant information of an existing user, and creates a new entry for a new user in the mongo
	collection "collection_tweet".

	:param collection_tweet: Name of the mongo collection to be updated.
	:param actorId: Twitter ID of the user.
	:param actorName: Actual Name of the user.
	:param actorScreenName: Screen Name of the user.
	:param tweetIds: Twitter Ids of the tweets currently being processed.
	:param hashTags: Hashtags occurring in the tweets currently being processed.
	:param atMentions: Atmentions occurring in the tweets currently being processed.
	:param urls: Urls occurring in the tweets currently being processed.
	:param keyWords: Keywords occurring in the tweets currently being processed.

	:return: NA
	'''
	if collection_tweet.find_one({'actorId': actorId}):
		hashTags = update_dictionary(hashTags, collection_tweet.find_one({'actorId': actorId})['hashTags'])
		atMentions = update_dictionary(atMentions, collection_tweet.find_one({'actorId': actorId})['atMentions'])
		urls = update_dictionary(urls, collection_tweet.find_one({'actorId': actorId})['urls'])
		tweetIds = collection_tweet.find_one({'actorId': actorId})['tweetIds'] + tweetIds
		keyWords = collection_tweet.find_one({'actorId': actorId})['keyWords'] + ['#'] + keyWords
		collection_tweet.update({'actorId': actorId}, {'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, \
									'hashTags': hashTags, 'atMentions': atMentions, 'urls': urls, 'tweetIds': tweetIds, 'keyWords': keyWords})
	else:
		hashTags = update_dictionary(hashTags, {})
		atMentions = update_dictionary(atMentions, {})
		urls = update_dictionary(urls, {})
		collection_tweet.insert_one({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'hashTags': hashTags, \
										'atMentions': atMentions, 'urls': urls, 'tweetIds': tweetIds, 'keyWords': keyWords})


def get_keywords(tweetTagged, cp, stopWords):
	'''
	This method takes a tagged tweet, stopwords and a parser, and returns all keywords from the tweet that are marked as
	"Nouns" by the tagger. The keywords are a group of nouns occurring together. Keywords from different tweets are separated
	by "#" since the original tweets are also separated by "#".

	:param tweetTagged: A tokenized and tagged tweet (or a collection of tweets separated by "#").
	:param cp: A parser.
	:param stopWords: Stop words from the English language.

	:return: A list of keywords (strings).
	'''
	ncp = cp.parse(tweetTagged)
	keyWords = []
	flag = 0
	for subTree in ncp.subtrees():
		if subTree.label() == 'Nouns':
			keyWordsTmp = ''
			for i in xrange(len(subTree)):
				kw = subTree[i][0]
				if kw not in stopWords and kw != 'RT' and len(kw) > 2 and not kw.startswith('#'):
					keyWordsTmp += kw
					keyWordsTmp += str(' ')
			keyWordsTmp = keyWordsTmp.rstrip()
			if keyWordsTmp:
				flag = 1
				keyWords.append(keyWordsTmp.lower())
		elif subTree.label() == 'Hash':
			kw = subTree[0][0]
			if kw == '#':
				keyWords.append(kw)
	if flag == 0:
		return []
	return keyWords


def tokenize_and_tag_nltk(tweet):
	'''
	This method tokenizes and tags a tweet using the "nltk" package.

	:param tweet: A tweet.

	:return: A tokenized and tagged tweet.
	'''
	tweetTokenized = nltk.word_tokenize(tweet)
	tweetTagged = nltk.pos_tag(tweetTokenized)
	return tweetTagged


def tokenize_and_tag_cmu(tweets):
	'''
	This method tokenizes and tags a tweet using the "cmu" tagger.

	:param tweet: A tweet.

	:return: A tokenized and tagged tweet.
	'''
	lock.acquire()

	f = open('tweet_input_tmp.txt', 'wb')
	f.write(tweets.encode('utf-8', 'ignore'))
	f.close()

	subprocess.call('ark-tweet-nlp-0.3.2/runTagger1.sh')

	tweetsTagged = []
	with open('output_tmp.txt') as f:
		for row in f:
			row = row.split()
			if len(row) >= 2:
				tweetsTagged.append((row[0], row[1]))
	f.close()

	os.remove('tweet_input_tmp.txt')
	os.remove('output_tmp.txt')

	lock.release()
	return tweetsTagged


def remove_entries_from_string(entries, string):
	'''
	This method removes a list of entries from a given string.

	:param entries: A list of strings.
	:param string: A given string.

	:return: An updated string.
	'''
	for entry in entries:
		string = re.sub(entry, '', string)
	return string


def get_hashtags_atmentions_urls(tweet):
	'''
	This method finds and returns all hashtags, atmentions and urls occurring in a given tweet.

	:param tweet: A tweet.

	:return: A list containing a list of hashtags, a list of atmentions and a list of urls occurring in the given tweet.
	'''
	hashTags = re.findall(r'#\w+', tweet)
	atMentions = re.findall(r'＠\w+', tweet) + re.findall(r'@\w+', tweet)
	urls = re.findall(r'(https?://[\w\d:#@＠%/;$()~_?\+-=\\\.&]+)', tweet)
	urls = [url.replace('.', '_') for url in urls]
	return [hashTags, atMentions, urls]


def valid_entry(collection_tweet, actorId, tweetId):
	'''
	This method checks if the given tweet of the given user has already been processed before.

	:param collection_tweet: An access client to the relevant mongo collection.
	:param actorId: Twitter ID of the user.
	:param tweetId: Twitter ID of the tweet.

	:return: A boolean True or False depending on whether the given tweet of the the given user was already processed or not.
	'''
	if collection_tweet.find_one({'actorId': actorId}):
		tweetIds = collection_tweet.find_one({'actorId': actorId})['tweetIds']
		if tweetId in tweetIds:
			return False
	return True


def compute_keywords_nltk(solrQuery, stopWords):
	'''
	This method takes a Solr query as an input, tokenizes and tags them using the "nltk" package, creates a grammar and
	a parser also using the "nltk" package, and uses them to parse keywords along with other relevant information from the tweets.

	:param solrQuery: Result of a Solr query for a given user.
	:param stopWords: Stop words of the English language.

	:return: A list containing a list of keywords, a list of tweet ids, a dictionary of hashtags, a dictionary of atmentions,
		a dictionary of urls, language, and user details like ID, name and screen name.
	'''
	# this grammar reads several possible ways that nouns can occur in a sentence and gives them the tag "Nouns", and it
	# also creates the tag "#" for reading an isolated #, which is used to separate tweets in a big string of all tweets
	grammar = r'''
		Nouns:  {<NN><NN>}
				{<NNS><NN>}
				{<NNP><NN>}
				{<NNPS><NN>}
				{<JJ><NN>}
				{<NN>}
				{<NN><NNS>}
				{<NNS><NNS>}
				{<NNP><NNS>}
				{<NNPS><NNS>}
				{<JJ><NNS>}
				{<NNS>}
				{<NN><NNP>}
				{<NNS><NNP>}
				{<NNP><NNP>}
				{<NNPS><NNP>}
				{<JJ><NNP>}
				{<NNP>}
				{<NN><NNPS>}
				{<NNS><NNPS>}
				{<NNP><NNPS>}
				{<NNPS><NNPS>}
				{<JJ><NNPS>}
				{<NNPS>}
		Hash:   {<\#>}
	'''
	# this creates a parser based on the above grammar using the "nltk" package
	cp = nltk.RegexpParser(grammar)

	actorId = ''
	actorName = ''
	actorScreenName = ''
	hashTags = []
	atMentions = []
	urls = []
	tweetIds = []
	keyWords = []
	language = ''
	# each tweet is processed one at a time and its keywords are extracted along with other relevant information like
	# hashtags, atmentions, urls and tweet ids.
	for query in solrQuery:
		actorId = query['actorId']
		actorName = query['actorName']
		actorScreenName = query['actorScreenName']
		tweet = query['contents']
		tweetId = query['id']
		language = query['lang']
		[hTags, atMens, urlinks] = get_hashtags_atmentions_urls(tweet)
		tweet = remove_entries_from_string(hTags + atMens + urlinks, tweet)
		hashTags.extend(hTags)
		atMentions.extend(atMens)
		urls.extend(urlinks)
		tweetIds.append(tweetId)
		if tweet:
			tweet = '# ' + tweet
			tweetTagged = tokenize_and_tag_nltk(tweet)
			kWords = get_keywords(tweetTagged, cp, stopWords)
			if kWords:
				keyWords.extend(kWords)

	hashTags = update_dictionary(hashTags, {})
	atMentions = update_dictionary(atMentions, {})
	urls = update_dictionary(urls, {})

	return [actorId, actorName, actorScreenName, hashTags, atMentions, urls, tweetIds, keyWords, language]


def compute_keywords_cmu(solrQuery, stopWords):
	'''
	This method takes a Solr query as an input, tokenizes and tags them using the "cmu" tagger, creates a grammar and a parser
	using the "nltk" package, and uses them to parse keywords along with other relevant information from the tweets.

	:param solrQuery: Result of a Solr query for a given user.
	:param stopWords: Stop words of the English language.

	:return: A list containing a list of keywords, a list of tweet ids, a dictionary of hashtags, a dictionary of atmentions,
		a dictionary of urls, language, and user details like ID, name and screen name.
	'''
	# this grammar reads a sequence of nouns following a sequence of adjectives and tags them as "Nouns", and it
	# also creates the tag "#" for reading an isolated #, which is used to separate tweets in a big string of all tweets
	grammar = r'''
		Nouns:  {<A>*<N>*<\^>+}
				{<A>*<\^>*<N>+}
		Hash:   {<\#>}
	'''
	# this creates a parser based on the above grammar using the "nltk" package
	cp = nltk.RegexpParser(grammar)

	tweets = ''
	actorId = ''
	actorName = ''
	actorScreenName = ''
	hashTags = []
	atMentions = []
	urls = []
	tweetIds = []
	language = ''
	# all the tweets are extracted and combined into a big string separated by "#". also, hashtags, atmentions, urls and
	# tweet ids are extracted in the process.
	for query in solrQuery:
		actorId = query['actorId']
		actorName = query['actorName']
		actorScreenName = query['actorScreenName']
		tweet = query['contents']
		tweetId = query['id']
		language = query['lang']
		[hTags, atMens, urlinks] = get_hashtags_atmentions_urls(tweet)
		tweets = tweets + ' # ' + tweet
		hashTags.extend(hTags)
		atMentions.extend(atMens)
		urls.extend(urlinks)
		tweetIds.append(tweetId)

	hashTags = update_dictionary(hashTags, {})
	atMentions = update_dictionary(atMentions, {})
	urls = update_dictionary(urls, {})

	keyWords = []
	if tweets:
		tweetsTagged = tokenize_and_tag_cmu(tweets)
		keyWords = get_keywords(tweetsTagged, cp, stopWords)

	return [actorId, actorName, actorScreenName, hashTags, atMentions, urls, tweetIds, keyWords, language]


def compute_keywords(solrQuery, stopWords, methodName):
	'''
	This is a wrapper function that relocates the keywords computation to the correct methods depending on the nlp package used.

	:param solrQuery: Results of a Solr Query for a given user.
	:param stopWords: Stopwords from the English language.
	:param methodName: The method used to parse tweets into relevant keywords. Default value is the 'cmu' tagger.
		Alternately, the 'nltk' package can also be used.

	:return: A list containing a list of keywords, a list of tweet ids, a dictionary of hashtags, a dictionary of atmentions,
		a dictionary of urls, language, and user details like ID, name and screen name.
	'''
	if methodName == 'nltk':
		return compute_keywords_nltk(solrQuery, stopWords)
	elif methodName == 'cmu':
		return compute_keywords_cmu(solrQuery, stopWords)
	else:
		print 'Wrong method name. Method name has to be either "nltk" or "cmu".'


def get_solr_query_for_user(screenName, timeFrame=90):
	'''
	This method queries solr for the given user and collects all tweets of the user for the last timeFrame number of days.

	:param screenName: Twitter screen name of the user.
	:param timeFrame: The number of days for which to collect all tweets of the user. Default value is 90 days.

	:return: The result of the Solr query.
	'''
	stopTime = int(time.time() * 1000)
	startTime = stopTime - timeFrame * 24 * 60 * 60 * 1000
	command = solr_url_staging + '/api/rest/v1/posts?startDate=%s&dataSrcs=TT&query=actorScreenName:\"%s\"&startRow=0&endDate=%s&rows=1000'\
        % (str(startTime), screenName, str(stopTime))
	response = requests.get(command)

	try:
		solrQuery = response.json()['results']
	except KeyError:
		solrQuery = False
	return solrQuery


def access_mongo_collection(client, database, collection):
	'''
	This method returns access to a mongo collection.

	:param client: An access client to a mongo database.
	:param database: Name of the mongo database.
	:param collection: Name of the mongo collection.

	:return: An access client to the specified mongo collection in the specified mongo database.
	'''
	return client[database][collection]


def access_mongo_database(url=''):
	'''
	This method returns a mongo client.

	:param url: The url of the mongo database.

	:return: An access client to the mongo database at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	if url:
		client = MongoClient(url)
	else:
		client = MongoClient()
	return client


# def update_tweets_for_specific_users_parallel(methodName = 'cmu', timeFrame = 90):
# 	client = access_mongo_database(mongo_api)
# 	collection_tweet = access_mongo_collection(client, topics_db, tweets_collection)
#
# 	stopWords = [line.lower().split() for line in open('stopwords.english')]
#
# 	threads = []
#
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('jtimberlake', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('justinbieber', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('anniekarni', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('katyperry', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('barackobama', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('taylorswift13', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('rihanna', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('ladygaga', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('britneyspears', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('cristiano', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('kimkardashian', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('jlo', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('arianagrande', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('oprah', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('nytimes', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('neymarjr', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('cnn', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('sportscenter', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('espn', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('srbachchan', collection_tweet, stopWords, methodName, timeFrame)))
# 	threads.append(threading.Thread(target=create_dbentry_for_user, args=('narendramodi', collection_tweet, stopWords, methodName, timeFrame)))
#
# 	for i in xrange(len(threads)):
# 		threads[i].start()
#
# 	for i in xrange(len(threads)):
# 		threads[i].join()
#
# 	client.close()
#
#
# def update_tweets_for_specific_users(methodName = 'cmu', timeFrame = 90):
# 	client = access_mongo_database(mongo_api)
# 	collection_tweet = access_mongo_collection(client, topics_db, tweets_collection)
#
# 	stopWords = [line.lower().split() for line in open('stopwords.english')]
#
# 	create_dbentry_for_user('jtimberlake', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('justinbieber', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('anniekarni', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('katyperry', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('barackobama', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('taylorswift13', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('rihanna', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('ladygaga', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('britneyspears', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('cristiano', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('kimkardashian', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('jlo', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('arianagrande', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('oprah', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('nytimes', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('neymarjr', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('cnn', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('sportscenter', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('espn', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('srbachchan', collection_tweet, stopWords, methodName, timeFrame)
# 	create_dbentry_for_user('narendramodi', collection_tweet, stopWords, methodName, timeFrame)
#
# 	client.close()


def create_dbentry_for_user(actorScreenName, stopWords, methodName, timeFrame, queue):
	'''
	This function is a wrapper function used in the method "update_tweets_for_all_users_parallel" so that the processing
	of each user in the current batch can be done in parallel threads, and the results are collected in a queue.

	:param actorScreenName: Twitter screen name of the user to be processed.
	:param stopWords: The stopwords from the English language.
	:param methodName: The method used to parse tweets into relevant keywords. Default value is the 'cmu' tagger.
		Alternately, the 'nltk' package can also be used.
	:param timeFrame: The number of days for which the tweets of a user is to be collected.
	:param queue: The queue required to collect the results of all the threads.

	:return: NA
	'''
	solrQuery = get_solr_query_for_user(actorScreenName, timeFrame)
	if solrQuery:
		queue.put(compute_keywords(solrQuery, stopWords, methodName))
	else:
		print 'No Solr Result for ' + actorScreenName + '.'


def update_tweets_for_all_users_parallel(methodName='cmu', timeFrame=90):
	'''
	This function goes through each user in the mongodb collection "collection_users", collects all tweets of that user
	from Solr for the last timeFrame number of days, computes the keywords from those tweets, and stores the keywords,
	hashtags and atmentions along with other relevant information of the user in a mongodb collection (specified here by
	"collection_tweet"). This is a parallel implementation of the method "update_tweets_for_all_users". Here the users
	are processed in batches, and in each batch a new thread is created for each user so that the processing of all the
	users in the current batch can be done in parallel.

	:param methodName: The method used to parse tweets into relevant keywords. Default value is the 'cmu' tagger.
		Alternately, the 'nltk' package can also be used.
	:param timeFrame: The number of days for which the tweets of a user is to be collected.

	:return: NA
	'''
	client = access_mongo_database(mongo_api)
	collection_users = access_mongo_collection(client, users_db, users_collection)
	collection_tweet = access_mongo_collection(client, topics_db, tweets_collection)

	# collect English stopwords from the file "stopwords.english"
	stopWords = [line.lower().split() for line in open('stopwords.english')]

	# define a queue required for collecting results of all threads
	manager = multiprocessing.Manager()
	queue = manager.Queue()

	m = 0
	batch_size = 30
	condition = True

	print 'Inserting into the Mongo collection'
	while condition:
		# get a batch of batch_size many users in each iteration from the mongo collection "collection_users"
		cursor = collection_users.find().skip(m * batch_size).limit(batch_size)
		if cursor.count(with_limit_and_skip=True) < batch_size:
			condition = False

		m = m + 1
		# if m >= 1:
		# 	condition = False

		threads = []
		# go through each user in the current batch
		for entry in cursor:
			# get the screen name of the user
			actorScreenName = entry['actorScreenName']
			# create a new thread for the user that collects all the tweets of the user for the last timeFrame number of days and compute the relevant keywords
			threads.append(threading.Thread(target=create_dbentry_for_user, args=(actorScreenName, stopWords, methodName, timeFrame, queue)))

		# start all the threads
		for i in xrange(len(threads)):
			threads[i].start()

		# wait for all the threads to finish
		for i in xrange(len(threads)):
			threads[i].join()

		# collect the results of all the threads together in the list "values"
		values = []
		while not queue.empty():
			entry = queue.get()
			if entry:
				actorId = entry[0]
				actorName = entry[1]
				actorScreenName = entry[2]
				hashTags = entry[3]
				atMentions = entry[4]
				urls = entry[5]
				tweetIds = entry[6]
				keyWords = entry[7]
				language = entry[8]
				values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'hashTags': hashTags, \
								'atMentions': atMentions, 'urls': urls, 'tweetIds': tweetIds, 'keyWords': keyWords, 'language': language})

		# store the relevant information in the mongo collection "collection_tweet" for all the users in the current batch
		if values:
			try:
				collection_tweet.insert_many(values)
			except BulkWriteError as bwe:
				pprint(bwe.details)

	print 'Indexing the Mongo collection'
	collection_tweet.create_index([('actorId', 1)])
	collection_tweet.create_index([('actorScreenName', 1)])
	collection_tweet.create_index([('language', 1)])

	client.close()


def update_tweets_for_all_users(methodName='cmu', timeFrame=90):
	'''
	This function goes through each user in the mongodb collection "collection_users", collects all tweets of that user
	from Solr for the last timeFrame number of days, computes the keywords from those tweets, and stores the keywords,
	hashtags and atmentions along with other relevant information of the user in a mongodb collection (specified here by
	"collection_tweet").

	:param methodName: The method used to parse tweets into relevant keywords. Default value is the 'cmu' tagger.
		Alternately, the 'nltk' package can also be used.
	:param timeFrame: The number of days for which the tweets of a user is to be collected.

	:return: NA
	'''
	client = access_mongo_database(mongo_api)
	collection_users = access_mongo_collection(client, users_db, users_collection)
	collection_tweet = access_mongo_collection(client, topics_db, tweets_collection)

	# collect English stopwords from the file "stopwords.english"
	stopWords = [line.lower().split() for line in open('stopwords.english')]

	m = 0
	batch_size = 5
	condition = True

	print 'Inserting into the Mongo collection'
	while condition:
		# get a batch of batch_size many users in each iteration from the mongo collection "collection_users"
		cursor = collection_users.find().skip(m * batch_size).limit(batch_size)
		if cursor.count(with_limit_and_skip=True) < batch_size:
			condition = False

		m = m + 1
		# if m >= 1:
		# 	condition = False

		values = []
		# go through each user in the current batch
		for entry in cursor:
			# get the screen name of the user
			actorScreenName = entry['actorScreenName']
			# get all the tweets of the user for the last timeFrame number of days
			solrQuery = get_solr_query_for_user(actorScreenName, timeFrame)
			if solrQuery:
				# compute the keywords from the tweets of the user
				entry = compute_keywords(solrQuery, stopWords, methodName)
				if entry:
					actorId = entry[0]
					actorName = entry[1]
					actorScreenName = entry[2]
					hashTags = entry[3]
					atMentions = entry[4]
					urls = entry[5]
					tweetIds = entry[6]
					keyWords = entry[7]
					language = entry[8]
					values.append({'actorId': actorId, 'actorName': actorName, 'actorScreenName': actorScreenName, 'hashTags': hashTags, \
									'atMentions': atMentions, 'urls': urls, 'tweetIds': tweetIds, 'keyWords': keyWords, 'language': language})
			else:
				print 'No Solr Result for ' + actorScreenName + '.'

		# store the relevant information in the mongo collection "collection_tweet" for all the users in the current batch
		if values:
			try:
				collection_tweet.insert_many(values)
			except BulkWriteError as bwe:
				pprint(bwe.details)

	print 'Indexing the Mongo collection'
	collection_tweet.create_index([('actorId', 1)])
	collection_tweet.create_index([('actorScreenName', 1)])
	collection_tweet.create_index([('language', 1)])

	client.close()



if __name__ == '__main__':
	timeFrame = 120
	methodName = 'cmu'

	start = (int)(time.time() * 1000)
	#update_tweets_for_all_users(methodName, timeFrame)
	update_tweets_for_all_users_parallel(methodName, timeFrame)
	stop = (int)(time.time() * 1000)
	print (stop - start)/1000.0