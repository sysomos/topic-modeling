# -*- coding: utf-8 -*-
__author__ = 'Koushik Pal'


'''
	This program takes a tweet and an integer N as inputs and returns the top N categories that match that tweet. The
	program finds keywords from the tweet and performs an elastic search of each keyword in Wikipedia with the remaining
	keywords forming a context. The elastic scores of the Wikipedia pages are normalized to a score between 0 and 1 so
	that they can be compared across several queries. Then the Wikipedia pages are sorted by their scores, and all the
	categories of the top M pages along with all their ancestors are computed. The categories are sorted by their
	frequencies and finally, the top N categories are returned as a json.
'''


import os
import nltk
import json
import itertools
import threading
import subprocess
from pprint import pprint
from pymongo import MongoClient
from collections import defaultdict
from elasticsearch import Elasticsearch


elastic_api = 'http://10.13.13.32:9200'
wiki_api = 'mongodb://10.13.13.32:27017'

wiki_db = 'wiki20151002'


topPageCount = 30
topCategoryCount = 100


# required for using the "cmu" tagger
lock = threading.Lock()



def categories_to_topcategories(categories, top_category_count=topCategoryCount):
	'''
	This method takes a list of categories, sorts them by their frequency count, and returns the top "top_category_count"
	many categories.

	:param categories: A list of categories.
	:param top_category_count: Number of top categories to return.

	:return: A list containing a dictionary of the frequency counts of the categories and the top categories.
	'''
	# create a dictionary of frequency counts of the categories in "categories"
	categories_dict = defaultdict(int)
	for category in categories:
		categories_dict[category] += 1
	# sort the categories by their frequency counts and return the top "top_category_count" many of them
	sorted_dict = sorted(categories_dict, key=categories_dict.get, reverse=True)
	top_categories = sorted_dict[:top_category_count]
	# return the top categories and the frequency counts of all categories
	return [categories_dict, top_categories]


def pages_to_categories(collection_wiki, pagesSorted):
	'''
	This method takes a list of Wikipedia pages and returns a list of all ancestors of their categories.

	:param collection_wiki: Access client to a mongo collection that stores the category hierarchy of Wikipedia.
	:param pagesSorted: A list of Wikipedia pages.

	:return: A list of all ancestors of all categories occurring in any of the Wikipedia pages in "pagesSorted".
	'''
	# collect all the categories occurring in any of the Wikipedia pages in "pagesSorted"
	categories = [page['_source']['category'] for page in pagesSorted]
	combined_categories = list(itertools.chain(*categories))
	combined_categories = [category.lower().replace('.', '_') for category in combined_categories]

	# find all ancestors of all the categories obtained above from the mongo collection "collection_wiki"
	fd = list(collection_wiki.find({'category': {'$in': combined_categories}}))

	cat = []
	[cat.extend(entry['ancestors_list']) for entry in fd]

	return cat


def normalize_scores_of_wikipages(pages):
	'''
	This method computes a normalized score between 0 and 1 for a given list of Wikipedia pages and their elastic search scores.

	:param pages: A list of Wikipedia pages along with their elastic search scores.

	:return: A list of Wikipedia pages with updated scores.
	'''
	sum = 0
	for page in pages:
		sum = sum + page['_score']
	for page in pages:
		page['_score'] = page['_score'] / sum
	return pages


def access_elastic_database(url=''):
	'''
	This method returns access to an elastic index.

	:param url: The url of the elastic index.

	:return: An access client to the elastic index at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	es_client = Elasticsearch(url)
	return es_client


def access_mongo_collection(client, database, collection):
	'''
	This method returns access to a mongo collection.

	:param client: An access client to a mongo database.
	:param database: Name of the mongo database.
	:param collection: Name of the mongo collection.

	:return: An access client to the specified mongo collection in the specified mongo database.
	'''
	return client[database][collection]


def access_mongo_database(url=''):
	'''
	This method returns a mongo client.

	:param url: The url of the mongo database.

	:return: An access client to the mongo database at the url specified. If no url is specified, an access client to "localhost" is returned.
	'''
	if url:
		client = MongoClient(url)
	else:
		client = MongoClient()
	return client


def get_top_pages_for_keywords(es_client, keyWords, top_page_count=topPageCount):
	'''
	This method computes the top Wikipedia pages for a given set of keywords. It does an elastic search query of each
	keyword on Wikipedia using the remaining keywords as a context. Then it normalizes the scores to a number between 0
	and 1. Finally, it sorts the Wikipedia pages using the new scores and returns the top "top_page_count" many Wikipedia
	pages.

	:param es_client: Access client to elastic search.
	:param keyWords: A list of keywords.
	:param top_page_count: Number of top Wikipedia pages to return.

	:return: A list of Wikipedia pages.
	'''
	pages_and_normalizedscores = []
	# go through each keyword in keyWords
	for entry in keyWords:
		# get the keyword
		keyword = entry
		# use the remaining keywords as a context
		context = [word for word in keyWords if word != keyword]
		# reduce the size of the context if it grows too big as elastic search does not allow a large context
		context = ','.join(context[:min(500, len(context))])
		# search the Wikipedia elastic database for the keyword and the context
		res = es_client.search(index=wiki_db,
								body={
									'query': {
										'bool': {
											'must': {'match': {'page.title': keyword}},
											'should': {
												'multi_match': {
													'query': context,
													'type': 'most_fields',
													'fields': ['page.title', 'page.text'],
													'minimum_should_match': '50%'
												}
											}
										}
									}
								})
		# from the results of the elastic search, discard pages that are redirects
		pages = [hit for hit in res['hits']['hits'] if hit['_source']['category'] and not hit['_source']['redirect']]
		if len(pages) > 0:
			# normalize the scores of the pages to a number between 0 and 1 for comparison with the results of other queries
			normalizedPages = normalize_scores_of_wikipages(pages)
			# store the pages along with their new scores in the list "pages_and_normalizedscores"
			pages_and_normalizedscores.extend(normalizedPages)

	# sort the Wikipedia pages by their scores and return the top "top_page_count" many of them
	pagesSorted = sorted(pages_and_normalizedscores, key=lambda x: x['_score'], reverse=True)
	pagesSorted = pagesSorted[:top_page_count]
	return pagesSorted


def update_top_categories_for_keywords(keyWords, top_page_count=topPageCount, top_category_count=topCategoryCount):
	'''
	This method computes the top categories for the given list of keywords.

	:param keyWords: A list of keywords (strings).
	:param top_page_count: Number of top Wikipedia pages to consider.
	:param top_category_count: Number of top categories to return.

	:return:  A list containing a dictionary of the frequency counts of the categories and the top categories for the given list of keywords.
	'''
	es_client = access_elastic_database(elastic_api)
	client_wiki = access_mongo_database(wiki_api)
	collection_wiki = access_mongo_collection(client_wiki, wiki_db, 'categories_ancestors')

	pagesSorted = get_top_pages_for_keywords(es_client, keyWords, top_page_count)
	categories = pages_to_categories(collection_wiki, pagesSorted)
	[categories_dict, top_categories] = categories_to_topcategories(categories, top_category_count)
	return [categories_dict, top_categories]


def get_keywords(tweetTagged, cp, stopWords):
	'''
	This method takes a tagged tweet, stopwords and a parser, and returns all keywords from the tweet that are marked as
	"Nouns" by the tagger. The keywords are a group of "Nouns" occurring together.

	:param tweetTagged: A tokenized and tagged tweet.
	:param cp: A parser.
	:param stopWords: Stop words from the English language.

	:return: A list of keywords (strings).
	'''
	ncp = cp.parse(tweetTagged)
	keyWords = []
	flag = 0
	for subTree in ncp.subtrees():
		if subTree.label() == 'Nouns':
			keyWordsTmp = ''
			for i in xrange(len(subTree)):
				kw = subTree[i][0]
				if kw not in stopWords and kw != 'RT' and len(kw) > 2 and not kw.startswith('#'):
					keyWordsTmp += kw
					keyWordsTmp += str(' ')
			keyWordsTmp = keyWordsTmp.rstrip()
			if keyWordsTmp:
				flag = 1
				keyWords.append(keyWordsTmp.lower())
	if flag == 0:
		return []
	print 'Key Words: ' + keyWords.__str__()
	return keyWords


def tokenize_and_tag_cmu(tweet):
	'''
	This method tokenizes and tags a tweet using the "cmu" tagger.

	:param tweet: A tweet.

	:return: A tokenized and tagged tweet.
	'''
	lock.acquire()

	f = open('tweet_input_tmp.txt', 'wb')
	f.write(tweet.encode('utf-8', 'ignore'))
	f.close()

	subprocess.call('ark-tweet-nlp-0.3.2/runTagger1.sh')

	tweetTagged = []
	with open('output_tmp.txt') as f:
		for row in f:
			row = row.split()
			if len(row) >= 2:
				tweetTagged.append((row[0], row[1]))
	f.close()

	os.remove('tweet_input_tmp.txt')
	os.remove('output_tmp.txt')

	lock.release()
	return tweetTagged



def compute_keywords_cmu(tweet, stopWords):
	'''
	This method takes a tweet as an input, tokenizes and tags them using the "cmu" tagger, creates a grammar and a
	parser using the "nltk" package, and uses them to parse keywords from the tweet.

	:param tweet: A string corresponding to a tweet.
	:param stopWords: Stop words of the English language.

	:return: A list of keywords (strings).
	'''
	# this grammar reads a sequence of nouns following a sequence of adjectives and tags them as "Nouns"
	grammar = r'''
		Nouns:  {<A>*<N>*<\^>+}
				{<A>*<\^>*<N>+}
	'''
	# this creates a parser based on the above grammar using the "nltk" package
	cp = nltk.RegexpParser(grammar)

	keyWords = []
	if tweet:
		tweetTagged = tokenize_and_tag_cmu(tweet)
		keyWords = get_keywords(tweetTagged, cp, stopWords)

	return keyWords


def get_top_categories(tweet, top_category_count=topCategoryCount):
	'''
	This method takes a tweet and an integer N as inputs and returns a json output of the top N categories related to
	the tweet.

	:param tweet: A string corresponding to a tweet.
	:param top_category_count: Number of top categories to return.

	:return: A json object containing the tweet and its top categories.
	'''
	stopWords = [line.lower().split() for line in open('stopwords.english')]
	keyWords = compute_keywords_cmu(tweet, stopWords)
	[categories_dict, top_categories] = update_top_categories_for_keywords(keyWords, top_category_count=top_category_count)
	return json.dumps({'tweet_keywords': keyWords, 'topCategories': top_categories}, encoding='utf-8')



if __name__ == "__main__":
	top_category_count = 50
	with open('solr_tweet.txt') as data_file:
		tweet = data_file.read()
		print('Tweet: ' + tweet)

	result_json = json.loads(get_top_categories(tweet, top_category_count))
	pprint(result_json)